package com.androidengineer.themes;

/**
 * Created by Alex on 26.05.2014.
 */
public interface Decorable {
    /** implements the concrete decorator's behavior */
    void decorate();
}
